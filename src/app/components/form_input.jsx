import React from "react";
import { withStyles, TextField } from '@material-ui/core';

const styles = theme => ({
	textField: {
		width: '100%',
		margin: 0,
		'& input[type=text]': {
			minHeight: 19
		},
		'& input[disabled]': {
			fontWeight: 700,
			color: '#111'
		}
	}
});

function RatesFormInput(props) {
	const { classes, name, disabled, label, onChange, value } = props;
	return (
		<TextField
			name={name}
			label={label}
			value={value}
			className={classes.textField}
			margin="normal"
			disabled={disabled}
			onInput={onChange}
		/>
	);
}

export default withStyles(styles)(RatesFormInput);