import React from "react";
import Spinner from "components/spinner";
import { withStyles, Select, FormControl, InputLabel } from '@material-ui/core';

const styles = theme => ({
	select: {
		width: '100%',
		'& > div': {
			position: 'relative',
			top: '-3px'
		},
		'& select': {
			minWidth: '100%',
			minHeight: 22
		},
		'& select:focus': {
			backgroundColor: 'transparent'
		}
	},
	selectLabel: {
		minWidth: '100%'
	},
	spinner: {
		width: '100%',
		textAlign: 'center'
	}
});

function RatesFormSelect(props) {
	const { classes, name, options, placeholder, disabled, onChange } = props;
	return (
		<FormControl className={classes.select} disabled={disabled}>
			<InputLabel className={classes.selectLabel} htmlFor="from-native-simple">{ disabled ? <div className={classes.spinner}><Spinner /></div> : placeholder }</InputLabel>
			<Select
				native
				onChange={onChange}
				inputProps={{
					name,
					id: `${name}-native-simple`
				}}
			>
				{options}
			</Select>
		</FormControl>
	);
}

export default withStyles(styles)(RatesFormSelect);