import React from "react";
import { faTimes, faSpinner } from "@fortawesome/fontawesome-free-solid";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";

const Spinner = props => <FontAwesomeIcon icon={'spinner'} pulse />;

export default Spinner;