import React from "react";
import autoBind from "react-autobind";
import { observer, inject } from 'mobx-react';
import { withStyles, Paper, Grid, Typography, Button, TextField, Select, FormControl, InputLabel } from '@material-ui/core';
import RatesFormSelect from  "components/form_select";
import RatesFormInput from  "components/form_input";
import Spinner from "components/spinner";
import { faCheck } from "@fortawesome/fontawesome-free-solid";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";

const styles = theme => ({
	root: theme.mixins.gutters({
		padding: theme.spacing.unit * 2,
		textAlign: 'center',
	}),
	paper: {
		padding: theme.spacing.unit * 2,
		paddingBottom: theme.spacing.unit * 3,
		color: theme.palette.text.secondary,
		[theme.breakpoints.down('sm')]: {
			width: '100%',
		},
		[theme.breakpoints.up('md')]: {
			width: 980,
		},
		display: 'inline-block'
	},
	form: {
		textAlign: 'left'
	},
	button: {
		marginTop: 'calc(10%)',
		minWidth: 50
	},
	title: {
		margin: 0,
		height: 'calc(100%)',
		paddingTop: 'calc(40%)',
		textAlign: 'right',
		color: theme.palette.primary.dark
	},
	headline: {
		marginBottom: 10,
		textTransform: 'uppercase',
		color: '#666',
		fontWeight: 300
	}
});

@inject("ratesStore")
@observer
class App extends React.Component {

	constructor() {
		super();
		autoBind(this);
	}

	componentWillMount() {
		this.props.ratesStore.refreshList();
	}

	onChange(e) {
		const name = e.target.name;
		let value = e.target.value;
		if (~['amountFrom'].indexOf(name)) {
			value = value.replace(/[^0-9.]/g, "");
			e.target.value = value;
		}
		this.props.ratesStore.setCalcValue(name, value);
	}

	onSubmit() {
		const { ratesStore } = this.props;
		ratesStore.setCalcReady.apply(ratesStore)
	}

	render() {
		const { ratesStore } = this.props;
		const { pending, rates, listSize, calculated } = ratesStore;
		const { classes } = this.props;
		let options = [<option key={"empty"} value={""} />];
		const isSubmitable = ratesStore.isCalcDataValid && !calculated.ready;
		const calculatedValue = ratesStore.calculatedValue;

		if (listSize) {
			options = [
				...options,
				...[...rates.keys()].map(key => {
					return <option key={key} value={key}>{rates.get(key).name}</option>
				})
			];
		}

		return (
			<div className={classes.root}>
				<Paper className={classes.paper}>
					<Typography variant="headline" className={classes.headline}>Cryptos rate converter</Typography>
					<Grid container spacing={24} className={classes.form}>
						<Grid item xs={1}>
							<Typography variant="title" className={classes.title}>FROM</Typography>
						</Grid>
						<Grid item xs={2}>
							<RatesFormInput name="amountFrom" label={'Amount'} disabled={pending} onChange={this.onChange} />
						</Grid>
						<Grid item xs={2}>
							<RatesFormSelect onChange={this.onChange} disabled={pending} placeholder={'Select crypto'} name={'from'} options={options}  />
						</Grid>
						<Grid item xs={1}>
							<Typography variant="title" className={classes.title}>TO</Typography>
						</Grid>
						<Grid item xs={2}>
							<RatesFormSelect onChange={this.onChange} disabled={pending} placeholder={'Select crypto'} name={'to'} options={options}  />
						</Grid>
						<Grid item xs={3}>
							<RatesFormInput value={calculatedValue ? `= ${calculatedValue}` : ''} name="amountTo" label={calculatedValue ? (<span> </span>) : '='} disabled={true} />
						</Grid>
						<Grid item xs={1} hidden={pending}>
							{ isSubmitable ?
								<Button onClick={this.onSubmit} variant="outlined" color="primary" elevation={1} className={classes.button}>{ pending ? <Spinner /> : <FontAwesomeIcon icon={'check'} /> }</Button>
								: null
							}
						</Grid>
					</Grid>

				</Paper>
			</div>
		)
	}
}

export default withStyles(styles)(App);