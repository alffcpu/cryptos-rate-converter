import appConfig from "../config";

class SourceAPI {

	static fetchListAsJSON() {
		return fetch(appConfig.API_URL)
			.then(response => response.json());
	}

}

export default SourceAPI;