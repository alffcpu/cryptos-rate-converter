class Utils {

	static getMultiplier(numbers) {
		let sizeAfter = 1;
		numbers.forEach((number) => {
			const splittedNumber = ("" + number).split('.');
			if (splittedNumber[1] && splittedNumber[1].length > sizeAfter) {
				sizeAfter = splittedNumber[1].length;
			}
		});
		return parseInt(`1${'0'.repeat(sizeAfter)}`);
	}

}

export default Utils;