import {observable, computed, action } from 'mobx';
import SourceAPI from "services/api";
import Utils from "services/utils";

class RatesStore {

	rates = new Map();
	@observable valid = false;
	@observable pending = false;
	@observable listSize = 0;
	@observable calculated = {
		ready: false,
		from: "",
		to: "",
		amountFrom: null
	};

	@action refreshList() {
		if (!this.valid) {
			this.pending = true;
			const promise = SourceAPI.fetchListAsJSON();
			promise.then(responseList => {
				let size = 0;
				responseList.forEach(currency => {
					const {id, symbol, name, rank, price_usd} = currency;
					this.rates.set(id, {symbol, name, rank, price_usd});
					size++;
				});
				this.listSize = size;
				this.valid = true;
				this.pending = false;
			})
			.catch(error => {
				this.pending = false;
				console.error(error);
			});
		}
	}

	@computed get isCalcDataValid() {
		let {from, to, amountFrom } = this.calculated;
		amountFrom = parseFloat(amountFrom);
		return (from !== "" && to !== "") &&
			(from !== to) &&
			(!isNaN(amountFrom) && amountFrom > 0);
	}

	@action setCalcValue(key, value) {
		this.calculated[key] = value;
		this.calculated.ready = false;
	}

	@action setCalcReady() {
		this.valid = false;
		this.refreshList();
		this.calculated.ready = true;
	}

	@computed get calculatedValue() {
		let {from, to, amountFrom, ready } = this.calculated;
		if (!this.pending && ready) {
			const cureencyFrom = this.rates.get(from);
			const cureencyTo = this.rates.get(to);
			if (cureencyFrom && cureencyTo) {
				const valueUSD = parseFloat(amountFrom) * cureencyFrom.price_usd;
				const valueTo = parseFloat(cureencyTo.price_usd);
				const multiplier = Utils.getMultiplier([valueUSD, valueTo]);
				return (valueUSD * multiplier) / (valueTo * multiplier) ;
			}
		}
		return '';
	}

}

const ratesStore = new RatesStore();
export default ratesStore;
export { RatesStore };