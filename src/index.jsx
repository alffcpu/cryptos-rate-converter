import "css/styles.css";
import React from "react";
import { render } from "react-dom";
import DevTools from "mobx-react-devtools";
import { Provider, onError } from 'mobx-react';
import ratesStore from "stores/rates";
import App from "app/index";

onError(error => {
	console.log('-onError-');
	console.log(error)
});

render(<div>
	<DevTools />
	<Provider {...{ratesStore}}>
		<App />
	</Provider>
</div>, document.querySelector('.rates-app'));