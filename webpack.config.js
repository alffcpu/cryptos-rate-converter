const path = require('path');
const webpack = require('webpack');

module.exports = {
	context: path.resolve(__dirname, "src"),
    entry: {
        index: './index.jsx'
    },
    output: {
        path: path.join(__dirname, './dist'),
        filename: 'bundle.js',

    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            exclude: /(node_modules|bower_components|public)/,
            use: "babel-loader"
        }, {
            test: /\.json$/,
            use: 'json-loader'
        }, {
	        test: /\.svg$/,
	        use: 'svg-loader?pngScale=2'
        }, {
			test: /\.css$/,
			use: ['style-loader', 'css-loader']
            }
        ]
    },
    resolve: {
        modules: [path.resolve(__dirname, "src"), 'node_modules'],
        extensions: ['.jsx', '.json', '.css', '.js'],
	    alias: {
		    components: path.resolve(__dirname, 'src/app/components/'),
		    stores: path.resolve(__dirname, 'src/app/stores/'),
		    services: path.resolve(__dirname, 'src/app/services/')
	    }
    },
	plugins: [],
	devtool: 'source-map'
};